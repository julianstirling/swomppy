import swomppy as sp
from IPython.display import HTML,display
import pyx
pyx.unit.set(defaultunit='mm')
import ipywidgets as widgets
from ipywidgets import interactive

def showsvg(canvas):
    canvas.writeSVGfile('/tmp/1.svg')
    display(HTML(open(f'/tmp/1.svg').read()))

def showpath(path):
    
    print_plot = pyx.canvas.canvas()
    if type(path) is sp.spath:
        path = [path]
    for p in path:
        print_plot.stroke(p.np,[pyx.color.rgb.blue, pyx.style.linewidth(0.3)])
    showsvg(print_plot)
    
def showlayer(layer):
    print_plot = pyx.canvas.canvas()
    if layer.brimPaths is not None:
        print_plot.stroke(layer.brimPaths.as_normpath(),[pyx.color.rgb.blue, pyx.style.linewidth(0.3)])
    print_plot.stroke(layer.allShells.as_normpath(),[pyx.color.rgb.green, pyx.style.linewidth(0.3)])
    print_plot.stroke(layer.fillPaths.as_normpath(),[pyx.color.rgb.red, pyx.style.linewidth(0.3)])
    showsvg(print_plot)

def interactiveview(thing):
    def showing(n):
        showlayer(thing.layers[n])
    int_view = interactive(showing,n=widgets.IntSlider(min=-0,max=len(thing.layers)-1,step=1,value=10))
    display(int_view)
