import numpy as np
from copy import deepcopy
import operator

from .spaths import spath,spathlist,sline,scurve
from .swomppy import tol,pdist,Params,version


def thingfromfile(filename,printsettings=None):
    if printsettings is None:
        printsettings = PrintSettings()
    else:
        assert type(printsettings) is PrintSettings, 'printsettings must be a PrintSettings object'
    print("I wish I did this. I will soon.")
    return None

def thingfromshells(shells,printsettings=None):
    if printsettings is None:
        printsettings = PrintSettings()
    else:
        assert type(printsettings) is PrintSettings, 'printsettings must be a PrintSettings object'
    N_lay = len(shells)
    layers = []
    for n,shell in enumerate(shells):
        if n==0:
            if printsettings.brim:
                brim_layer=True
            else:
                brim_layer=False
        else:
            brim_layer=False
        
        if n==0 or n==N_lay-1:
            infillType=Params.VertInfill
            infillSpacing=printsettings.offset
        elif n==1 or n==N_lay-2:
            infillType=Params.HorInfill
            infillSpacing=printsettings.offset
        else:
            infillType=Params.CrossInfill
            infillSpacing=3
            
        
        layer = Layer(shell,offset=printsettings.offset,infillSpacing=infillSpacing,infillType=infillType,brim=brim_layer)
        layer.fillLayer()
        layers.append(layer)
    
    return Thing(layers,printsettings)

class Thing(object):
    """A thing is a set of layers, and layers are a set of spaths"""
    
    def __init__(self,layers,printsettings=None):
        if printsettings is None:
            printsettings = PrintSettings()
        else:
            assert type(printsettings) is PrintSettings, 'printsettings must be a PrintSettings object'
        self.printsettings = printsettings
        self.layers=layers

#Object for a layer, can do things like fill itself.
class Layer(object):
    
    def __init__(self,shell,offset=0.35,numInset=3,infillSpacing=None,infillType=Params.CrossInfill,brim=False):
        self.offset=offset
        self.numInset=numInset
        if infillSpacing is None:
            self.infillSpacing=offset
        else:
            self.infillSpacing=infillSpacing
        self.infillType=infillType
        
        #Paths to populate
        self.brimPaths = None
        self.allPaths = spathlist()
        self.fillPaths = spathlist()
        
        #ALL!!
        self.allShells = spathlist()
        
        #Specific paths
        self.innermostShell=None
        
        if type(shell) is spath:
            shell = spathlist(shell)
        
        assert type(shell) is spathlist, "Layer perimeter should be an spath or spathlist"
        
        for path in shell:
            assert type(path) is spath, "Layer perimeter should be an spath or a list of spaths"
            #check path starts and finishes at same place
            assert path.closed(), "all paths in outer shell of layer should be closed"

        
        self.outerShell=deepcopy(shell)
        self.outerShell.checkPathDirsValid(fix=True)
        
        if brim:
            self.brimPaths = spathlist()
            brim = self.outerShell.insetPath(-5*self.offset)
            
            for path in brim:
                self.brimPaths.append(path)
                self.allPaths.append(path)
            for n in range(4):
                brim = brim.insetPath(self.offset)
                for path in brim:
                    self.brimPaths.append(path)
                    self.allPaths.append(path)
        #Save outer shell
        for path in self.outerShell:
            self.allShells.append(path)
            self.allPaths.append(path)
    
    #Fill layer based on defined infill parameters
    def fillLayer(self):
        #Do inset fills
        shell = self.outerShell
        for n in range(self.numInset):
            shell = shell.insetPath(self.offset)
            if len(shell) ==0:
                return
            for subpath in shell:
                self.allShells.append(subpath)
                self.allPaths.append(subpath)

        #innermostShell is inset of the printed innermost shell to stop fill lines overlapping too far
        self.innermostShell= shell.insetPath(.75*self.offset)
        if len(self.innermostShell) == 0:
            return
        #Do hatched fill inside this shell
        if self.infillType in [Params.VertInfill,Params.CrossInfill]:
            fillLines = self.innermostShell.generateInFill(spacing = self.infillSpacing)
            for subpath in fillLines:
                self.fillPaths.append(subpath)
                self.allPaths.append(subpath)
        if self.infillType in [Params.HorInfill,Params.CrossInfill]:
            fillLines = self.innermostShell.generateInFill(spacing = self.infillSpacing,vert=False)
            for subpath in fillLines:
                self.fillPaths.append(subpath)
                self.allPaths.append(subpath)
    
    def genxyPaths(self,pathobj='allPaths'):
        xyPaths = []
        if pathobj=='allPaths':
            pathobj=self.allPaths
        elif pathobj=='brimPaths':
            pathobj=self.brimPaths
        elif pathobj=='allShells':
            pathobj=self.allShells
        elif pathobj=='fillPaths':
            pathobj=self.fillPaths
            
            
        if pathobj==None:
            return []
        for subpath in pathobj:
            xyPath=[]
            xyPath.append(subpath.start())
            for item in subpath:
                if type(item)==sline:
                    pts = [item.end()]
                elif type(item)==scurve:
                    pts = item.xypoints(1.5*self.offset)
                    #don't need first point as it is a repition of previous points
                    pts = pts[1:]
                else:
                    assert False, "Elements of this spath are not sline or scurves!"
                    
                if 'NoPrint' in item.attrs:
                    #If not printing this item. End this path and start a new one
                    xyPaths.append(xyPath)
                    xyPath = [pts[-1]]
                else:
                    for pt in pts:
                            xyPath.append(pt)
            xyPaths.append(xyPath)
        return xyPaths

def orderPrintPath(pths):
    if len(pths)<2:
        return pths
    printPath = [pths.pop(0)]
    starts = [p[0] for p in pths]
    ends = [p[-1] for p in pths]

    while len(pths)>0:
        s_ind, s_dist = min(enumerate([pdist(printPath[-1][-1],s) for s in starts]), key=operator.itemgetter(1))
        e_ind, e_dist = min(enumerate([pdist(printPath[-1][-1],e) for e in ends]), key=operator.itemgetter(1))

        if s_dist<e_dist:
            printPath.append(pths.pop(s_ind))
            starts.pop(s_ind)
            ends.pop(s_ind)
        else:
            printPath.append(list(reversed(pths.pop(e_ind))))
            starts.pop(e_ind)
            ends.pop(e_ind)
    return printPath

# a path for printing, contanins path parameters such as poistions extrusion and type
class PrintPath():
    N_PTH_TYPES = 5
    PTH_NORMAL = 0
    PTH_OUTER = 1
    PTH_BOTTOM = 2
    PTH_TOP = 3
    PTH_FLEX = 4
    
    def __init__(self,pths=None,sep=.6):
        self.sep=sep
        self.pathEmpty=True
        if pths is None:
            self.X=np.array(0)
            self.Y=np.array(0)
            self.E=np.array(0,dtype=bool)
            self.Ty=np.array(0,dtype=int)
        else:
            self.catPaths(pths)
    
    def catPaths(self,pths):
        for n in range(len(pths)):
            pth = pths[n]
            x = pth.X
            y = pth.Y
            e = pth.E
            ty = pth.Ty
            self.appendPath(x,y,e,ty)
                    
    def appendPath(self,x,y,e,ty=None):
        if ty is None:
            ty=np.ones(x.shape,dtype=int)*self.PTH_NORMAL
        if self.pathEmpty is True:
            e[0]=False
            self.X=np.copy(x)
            self.Y=np.copy(y)
            self.E=np.copy(e)
            self.Ty=np.copy(ty)
            self.pathEmpty=False
        else:
            d=np.linalg.norm([self.X[-1]-x[0],self.Y[-1]-y[0]])
            if d>self.sep:
                e[0]=False
            self.X = np.concatenate((self.X,x))
            self.Y = np.concatenate((self.Y,y))
            self.E = np.concatenate((self.E,e))
            self.Ty = np.concatenate((self.Ty,ty))
                
    def genLines(self,points,pthType=PTH_NORMAL,extrude=True):
        x=[]
        y=[]
        e=[]
        ty=[]
        for point in points:
            x.append(point[0])
            y.append(point[1])
            e.append(True)
            ty.append(pthType)
        x=np.asarray(x)
        y=np.asarray(y)
        e=np.asarray(e)
        ty=np.asarray(ty)
        self.appendPath(x,y,e,ty)

    def genCirc(self,xc,yc,r,ts,te,N=16,pthType=PTH_NORMAL,extrude=True):
        x=[]
        y=[]
        while ts>te:
            te+=360
        a_step = float(te-ts)/(N-1)
        for n in range(N):
            a_rad = np.pi/180.0*(ts+n*a_step)
            x.append(xc+r*np.cos(a_rad))
            y.append(yc+r*np.sin(a_rad))
        x=np.asarray(x)
        t=np.asarray(y)
        if extrude:
            e = np.ones(N,dtype=bool)
        else:
            e = np.zeros(N,dtype=bool)
        ty = np.ones(N,dtype=int)*pthType
        self.appendPath(x,y,e,ty)
    
    def offset(self,x,y):
        self.X=self.X+x
        self.Y=self.Y+y

class PrintSettings():
    
    def __init__(self,flavor='Marlin', h0=.125, h=.25,offset=0.35,brim=True):
        
        self.flavor = flavor
        self.h0 = h0
        self.h = h
        self.offset = 0.35
        self.brim=brim
        
        assert flavor in CodeGen.acceptedFlavors, 'Unrecognised g-code flavor. Use %s'%str(CodeGen.acceptedFlavors)
        if flavor=='UltiGCode':
            self.fname_pre = 'sp_UM_'
            self.w_ratio=1.4
        else:
            self.fname_pre = 'sp_Mar_'
            self.w_ratio=1.2
        
        self.MSpeed = list(range(PrintPath.N_PTH_TYPES))
        self.ESpeed = list(range(PrintPath.N_PTH_TYPES))
        
        #Set default speeds
        self.ESpeed[PrintPath.PTH_NORMAL] = 50 
        self.ESpeed[PrintPath.PTH_OUTER] = 30
        self.ESpeed[PrintPath.PTH_BOTTOM] = 20
        self.ESpeed[PrintPath.PTH_TOP] = 20
        self.ESpeed[PrintPath.PTH_FLEX] = 20
        
        self.MSpeed[PrintPath.PTH_NORMAL] = 120
        self.MSpeed[PrintPath.PTH_OUTER] = 120
        self.MSpeed[PrintPath.PTH_BOTTOM] = 72
        self.MSpeed[PrintPath.PTH_TOP] = 72
        self.MSpeed[PrintPath.PTH_FLEX] = 72
        
    def setESpeed(self,ptype,speed):
        self.ESpeed[ptype] = speed
    
    def setMSpeed(self,ptype,speed):
        self.MSpeed[ptype] = speed
        
    def calcnumlayers(self, height):
        return int((height-self.h0)//self.h)


def generateGcode(thing,name,prependID=True):

    assert type(name) is str, 'Print name must be a string'
    
    if prependID:
        fname = thing.printsettings.fname_pre+name+'.gcode'
    else:
        fname = name+'.gcode'
    
    gfile = CodeGen(fname,flavor=thing.printsettings.flavor)
    gfile.writeHeader()
    gfile.writeGcode(thing)
    gfile.writeFooter()

class CodeGen():
    acceptedFlavors=['UltiGCode','Marlin']
    
    
    def __init__(self,filename,flavor='UltiGCode'):
        self.f=open(filename,'w')
        self.x=0
        self.y=0
        self.ex=0
        self.z=0
        self.LayersWritten=0
        assert flavor in self.acceptedFlavors, "Gcode flavor '%s' not supported."%flavor
        self.flavor = flavor
    
    def retract(self):
        if self.flavor=='UltiGCode':
            return 'G10'
        elif self.flavor=='Marlin':
            self.ex-=4
            return 'G1 F2100 E%.5f'%self.ex
    
    def recover(self):
        if self.flavor=='UltiGCode':
            return 'G11'
        elif self.flavor=='Marlin':
            self.ex+=4
            return 'G1 F2100 E%.5f'%self.ex
    
    def write(self,text,LineFeed=True,Flush=False):
        self.f.write('%s\n'%text)
        if Flush:
            self.flush()
        
    def flush(self):
        self.f.flush()
    
    def writeHeader(self,nozzle=.4):
        if self.flavor=='UltiGCode':
            self.write(''';FLAVOR:UltiGCode
;Generated in swommpy %s
;MATERIAL:155585
;MATERIAL2:0
;NOZZLE_DIAMETER:%.1f
M82 ; absolute extrusion mode
M10\n\n'''%(version(),nozzle),LineFeed=False)
        elif  self.flavor=='Marlin':
            self.write(''';FLAVOR:Marlin
;Generated in swommpy %s
M190 S70
M104 S189
M109 S189
M82 ;absolute extrusion mode
G28 ;Home
G1 Z15.0 F6000 ;Move the platform down 15mm
;Prime the extruder
G92 E0
G1 F200 E3
G92 E0\n\n'''%(version()),LineFeed=False)
        
    def writeFooter(self):
        if self.flavor=='UltiGCode':
            self.write('''%s
%s
%s
M107
M82
;G-code OVER!'''%(self.retract(),self.retract(),self.retract()),LineFeed=False)
        elif  self.flavor=='Marlin':
            self.write('''%s
M107
M104 S0
M140 S0
G28 X0 Y0
M84
M82 ;absolute extrusion mode
M104 S0
;G-code OVER!'''%self.retract(),LineFeed=False)
        self.flush()

    def writeGcode(self,thing):
        layers = thing.layers
        printsettings = thing.printsettings
        
        N_lay = len(layers)
        for n,layer in enumerate(layers):
            w=printsettings.w_ratio*layer.offset
            h=printsettings.h
            h0 = printsettings.h0
            pth = PrintPath()
            if n==0:
                linetype = pth.PTH_BOTTOM
                circtype = pth.PTH_BOTTOM
            elif n==N_lay-1:
                linetype = pth.PTH_TOP
                circtype = pth.PTH_TOP
            else:
                linetype = pth.PTH_NORMAL
                circtype = pth.PTH_NORMAL

            printPath = orderPrintPath(layer.genxyPaths(pathobj='brimPaths'))
            for printLine in orderPrintPath(layer.genxyPaths(pathobj='allShells')):
                printPath.append(printLine)
            for printLine in orderPrintPath(layer.genxyPaths(pathobj='fillPaths')):
                printPath.append(printLine)

            for xyPath in printPath:
                pth.genLines(xyPath,pthType=linetype)
            pth.offset(85,85)

            if n==0:
                self.writeLayer(pth,w,h0,settings=printsettings,exFrac=1)
            elif n==N_lay-2:
                self.writeLayer(pth,w,h,settings=printsettings,exFrac=w/h)
            elif n==N_lay-1:
                self.writeLayer(pth,w,h,settings=printsettings,exFrac=1)
            else:
                self.writeLayer(pth,w,h,settings=printsettings,exFrac=.7)


    def writeLayer(self,pth,w,h,settings=None,exFrac=1):
        n=self.LayersWritten
        #TODO: improve machine/profile specific elements
        if settings is None:
            settings=PrintSpeeds()
        self.z+=h
        ex_per_d = exFrac*(w-h)*h+np.pi*(h/2)**2
        retracted = False
        first=True
        self.write(';Layer %d'%n)
        if not n==0:
            self.write('M106 S255')
        for x,y,e,ty in zip(pth.X,pth.Y,pth.E,pth.Ty):
            if e:
                if retracted==True:
                    self.write(self.recover())
                    retracted=False
                D = np.linalg.norm([self.x-x,self.y-y])
                self.ex += ex_per_d*D
                self.write('G1 F%.0f X%.3f Y%.3f E%.5f'%(settings.ESpeed[ty]*60,x,y,self.ex))
                
            else:
                
                if retracted==False:
                    D = np.linalg.norm([self.x-x,self.y-y])
                    if D>2:
                        self.write(self.retract())
                        retracted=True
                if first:
                    self.write('G0 F%.0f X%.3f Y%.3f Z%.3f'%(settings.MSpeed[ty]*60,x,y,self.z))
                    first = False
                else:
                    self.write('G0 F%.0f X%.3f Y%.3f'%(settings.MSpeed[ty]*60,x,y))
                
            self.x=x
            self.y=y
        self.LayersWritten+=1
    

