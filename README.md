#Swomppy
## Slicer With Optimised Mechanical Performance in Python

Swomppy is (or will be!!) a slicer for 3D printers where you can optimise for specific mechanical properties of an object rather than trying get the shape exactly as input. Swomppy is not designed to ever be good enough for the for the *prime-time*. It is a simple test bed to test new slicing algorithms.

The initial test case for Swomppy is flexure hinges.